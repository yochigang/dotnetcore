using Microsoft.EntityFrameworkCore;
using Dotnetcore.webapi.Data;
using Dotnetcore.webapi.Data.Models;
using Dotnetcore.webapi.Data.Requests;
using Dotnetcore.webapi.Interfaces;

namespace Dotnetcore.webapi.Repositories;

public class CatRepository : ICatRepository
{
    private readonly ApplicationDbContext _context;

    public CatRepository(ApplicationDbContext context)
        => _context = context;

    public async Task<IEnumerable<Cat>> Fetch()
    {
        var entities = await _context.Cats
            .ToListAsync();

        return entities;
    }

    public async Task<Cat?> Find(long id)
    {
        var entity = await _context.Cats
            .FindAsync(id);

        return entity;
    }

    public async Task<Cat> Create(CatRequest model)
    {
        var entity = new Cat
        {
            Id = model.Id,
            CatName = model.CatName,
            CatDate = model.CatDate
        };

        _context.Cats.Add(entity);
        await _context.SaveChangesAsync();

        return entity;
    }

    public async Task<Cat> Update(Cat entity, CatRequest model)
    {
        entity.CatName = model.CatName;
        entity.CatDate = model.CatDate;
        await _context.SaveChangesAsync();

        return entity;
    }

    public async Task<long> Delete(Cat entity)
    {
        _context.Cats.Remove(entity);
        var rowAffected = await _context.SaveChangesAsync();

        return rowAffected;
    }
}
