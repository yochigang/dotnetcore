using Microsoft.EntityFrameworkCore;
using Dotnetcore.webapi.Data;
using Dotnetcore.webapi.Data.Models;
using Dotnetcore.webapi.Data.Requests;
using Dotnetcore.webapi.Interfaces;

namespace Dotnetcore.webapi.Repositories;

public class BooktypeRepository : IBooktypeRepository
{
    private readonly ApplicationDbContext _context;

    public BooktypeRepository(ApplicationDbContext context)
        => _context = context;

    public async Task<IEnumerable<Booktype>> Fetch()
    {
        var entities = await _context.Booktypes
            .ToListAsync();

        return entities;
    }

    public async Task<Booktype?> Find(long id)
    {
        var entity = await _context.Booktypes
            .FindAsync(id);

        return entity;
    }

    public async Task<Booktype> Create(BooktypeRequest model)
    {
        var entity = new Booktype
        {
            Id = model.Id,
            BooktypeName = model.BooktypeName,
           
        };

        _context.Booktypes.Add(entity);
        await _context.SaveChangesAsync();

        return entity;
    }

    public async Task<Booktype> Update(Booktype entity, BooktypeRequest model)
    {
        entity.BooktypeName = model.BooktypeName;
        await _context.SaveChangesAsync();

        return entity;
    }

    public async Task<long> Delete(Booktype entity)
    {
        _context.Booktypes.Remove(entity);
        var rowAffected = await _context.SaveChangesAsync();

        return rowAffected;
    }
}
