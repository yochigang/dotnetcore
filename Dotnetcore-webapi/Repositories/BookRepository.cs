using Microsoft.EntityFrameworkCore;
using Dotnetcore.webapi.Data;
using Dotnetcore.webapi.Data.Models;
using Dotnetcore.webapi.Data.Requests;
using Dotnetcore.webapi.Interfaces;

namespace Dotnetcore.webapi.Repositories;

public class BookRepository : IBookRepository
{
    private readonly ApplicationDbContext _context;

    public BookRepository(ApplicationDbContext context)
        => _context = context;

    public async Task<IEnumerable<Book>> Fetch()
    {
        var entities = await _context.Books
            .Include(m => m.Booktype)
            .ToListAsync();

        return entities;
    }

    public async Task<IEnumerable<Book>> FetchByBooktypeId(long BooktypeId)
    {
        var entities = await _context.Books
            .Include(m => m.Booktype)
            .Where(m => m.Booktype.Id == BooktypeId)
            .ToListAsync();

        return entities;
    }

    public async Task<Book?> Find(long id)
    {
        var entity = await _context.Books
        .Include(m => m.Booktype)
        .SingleOrDefaultAsync(m => m.Id == id);
        //var entity = await _context.Books
        //.FindAsync(id);

        return entity;
    }

    public async Task<Book> Create(BookRequest model)
    {
        var entity = new Book
        {
            Id = model.Id,
            BookName = model.BookName,
            BooktypeId = model.BooktypeId,
        };

        _context.Books.Add(entity);
        await _context.SaveChangesAsync();

        return entity;
    }

    public async Task<Book> Update(Book entity, BookRequest model)
    {
        entity.BookName = model.BookName;
        entity.BooktypeId = model.BooktypeId;
        await _context.SaveChangesAsync();

        return entity;
    }

    public async Task<long> Delete(Book entity)
    {
        _context.Books.Remove(entity);
        var rowAffected = await _context.SaveChangesAsync();

        return rowAffected;
    }
}
