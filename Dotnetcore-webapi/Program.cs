using Dotnetcore.webapi.Configurations;
using Dotnetcore.webapi.Services;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.ConfigureDbContext(configuration);
builder.Services.ConfigureValidation();
builder.Services.AddSwaggerGen();
builder.Services.ConfigureDependencyInjection();
builder.Services.ConfigureSwagger(configuration);
builder.Services.AddSingleton<ILoggerManagerService, LoggerManagerService>(); //1

var app = builder.Build();
var logger = app.Services.GetRequiredService<ILoggerManagerService>(); //2

app.ConfigureExceptionHandler(logger); //3

// Configure the HTTP request pipeline.
// if (app.Environment.IsDevelopment())
// {
//     app.UseSwagger();
//     app.UseSwaggerUI();
// }

app.UseOpenApi(configuration);

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
