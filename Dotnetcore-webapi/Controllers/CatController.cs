using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Dotnetcore.webapi.Data.Requests;
using Dotnetcore.webapi.Data.Responses;
using Dotnetcore.webapi.Extensions;
using Dotnetcore.webapi.Interfaces;
using Dotnetcore.webapi.Services;

namespace Dotnetcore.webapi.Controllers;

[Route("api/[controller]")]
[ApiController]
[ApiExplorerSettings(GroupName = "Cat")]
[Produces("application/json")]
public class CatController : ControllerBase
{
    private readonly ICatRepository _catRepository;
    private readonly IValidator<CatRequest> _validator;
    private readonly ILoggerManagerService _logger;

    public CatController(ICatRepository catRepository,
        IValidator<CatRequest> validator,
        ILoggerManagerService logger
        )
    {
        _catRepository = catRepository;
        _validator = validator;
        _logger = logger;
    }


    /// <summary>
    /// แสดงข้อมูลรายการ CAT ทั้งหมด
    /// </summary>
    /// <returns>ข้อมูลรายการ CAT ทั้งหมด</returns>
    /// <response code="200">ส่งข้อมูล CAT ทั้งหมด</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="422">เกิดข้อผิดพลาดจากข้อมูลที่ส่งมา</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpGet]
    [ProducesResponseType(typeof(List<CatResponse>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status422UnprocessableEntity)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Get()
    {

            var cats = await _catRepository.Fetch();
             //throw new Exception("Exception while fetching all the students from the storage.");
            _logger.LogInfo($"{StatusCodes.Status200OK} - {cats.Count()} retrieved");
            return Ok(cats.Select(m => m.ToResponse()).ToList());

    }

    /// <summary>
    /// แสดงข้อมูล CAT ที่ระบุ
    /// </summary>
    /// <param name="id"></param>
    /// <returns>ข้อมูล CAT ที่ระบุ</returns>
    /// <response code="200">ส่งข้อมูล CAT ที่ระบุ</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="404">ไม่พบข้อมูลที่ระบุ</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpGet("{id}")]
    [ProducesResponseType(typeof(CatResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Get(long id)
    {
        // try
        // {
            var cat = await _catRepository.Find(id);
            if (cat is null)
            {
                _logger.LogError($"{StatusCodes.Status404NotFound} - Not found");
                return NotFound();
            }

            _logger.LogInfo($"{StatusCodes.Status200OK} - Get the Item");
            return Ok(cat.ToResponse());
        // }
        // catch (Exception ex)
        // {
        //     _logger.LogError($"{StatusCodes.Status500InternalServerError} - {ex.Message}");
        //     return StatusCode(StatusCodes.Status500InternalServerError);
        // }
    }


    /// <summary>
    /// เพิ่มข้อมูล CAT
    /// </summary>
    /// <param name="model"></param>
    /// <returns>ข้อมูล CAT ที่เพิ่มแล้ว</returns>
    /// <response code="201">ส่งข้อมูล CAT ที่เพิ่มแล้ว</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="422">เกิดข้อผิดพลาดจากข้อมูลที่ส่งมา</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpPost]
    [ProducesResponseType(typeof(CatResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status422UnprocessableEntity)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Post([FromBody] CatRequest model)
    {
        var validationResult = await _validator.ValidateAsync(model);
        if (!validationResult.IsValid) 
        {
            _logger.LogError($"{StatusCodes.Status422UnprocessableEntity} - Invalid input");
            return UnprocessableEntity(validationResult.ToDictionary());
        }

        // try
        // {
            var cat = await _catRepository.Create(model);
            _logger.LogInfo($"{StatusCodes.Status201Created} - The item was created");
            return CreatedAtAction(nameof(Post), cat.ToResponse());
        // }
        // catch (Exception ex)
        // {
        //     _logger.LogError($"{StatusCodes.Status500InternalServerError} - {ex.Message}");
        //     return StatusCode(StatusCodes.Status500InternalServerError);
        // }
    }


    /// <summary>
    /// แก้ไขข้อมูล CAT
    /// </summary>
    /// <param name="id"></param>
    /// <param name="model"></param>
    /// <returns>ข้อมูล CAT ที่แก้ไขแล้ว</returns>
    /// <response code="200">ส่ง CAT ที่แก้ไขแล้ว</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="404">ไม่พบข้อมูลที่ระบุ</response>
    /// <response code="422">เกิดข้อผิดพลาดจากข้อมูลที่ส่งมา</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpPut("{id}")]
    [ProducesResponseType(typeof(CatResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status422UnprocessableEntity)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Put(long id, [FromBody] CatRequest model)
    {
        var validationResult = await _validator.ValidateAsync(model);
        if (!validationResult.IsValid) 
        {
            _logger.LogError($"{StatusCodes.Status422UnprocessableEntity} - Invalid input");
            return UnprocessableEntity(validationResult.ToDictionary());
        }

        // try
        // {
            var source = await _catRepository.Find(id);
            if (source is null)
            {
                _logger.LogError($"{StatusCodes.Status404NotFound} - Not found");
                return NotFound();
            }

            var cat = await _catRepository.Update(source, model);
            _logger.LogInfo($"{StatusCodes.Status200OK} - The item was updated");
            return Ok(cat.ToResponse());
        // }
        // catch (Exception ex)
        // {
        //     _logger.LogError($"{StatusCodes.Status500InternalServerError} - {ex.Message}");
        //     return StatusCode(StatusCodes.Status500InternalServerError);
        // }
    }


    /// <summary>
    /// ลบข้อมูล  CAT
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    /// <response code="204">ลบข้อมูล CAT</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="404">ไม่พบข้อมูลที่ระบุ</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Delete(long id)
    {
        // try
        // {
            var cat = await _catRepository.Find(id);
            if (cat is null)
            {
                _logger.LogError($"{StatusCodes.Status404NotFound} - Not found");
                return NotFound();
            }

            await _catRepository.Delete(cat);
            _logger.LogInfo($"{StatusCodes.Status204NoContent} - The item was deleted");
            return NoContent();
        // }
        // catch (Exception ex)
        // {
        //     _logger.LogError($"{StatusCodes.Status500InternalServerError} - {ex.Message}");
        //     return StatusCode(StatusCodes.Status500InternalServerError);
        // }
    }
}
