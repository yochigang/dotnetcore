using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Dotnetcore.webapi.Data.Requests;
using Dotnetcore.webapi.Data.Responses;
using Dotnetcore.webapi.Extensions;
using Dotnetcore.webapi.Interfaces;
using Dotnetcore.webapi.Services;

namespace Dotnetcore.webapi.Controllers;

[Route("api/[controller]")]
[ApiController]
[ApiExplorerSettings(GroupName = "Book")]
[Produces("application/json")]
public class BookController : ControllerBase
{
    private readonly IBookRepository _bookRepository;
    private readonly IValidator<BookRequest> _validator;
    private readonly ILoggerManagerService _logger;

    public BookController(IBookRepository bookRepository,
        IValidator<BookRequest> validator,
        ILoggerManagerService logger
        )
    {
        _bookRepository = bookRepository;
        _validator = validator;
        _logger = logger;
    }


    /// <summary>
    /// แสดงข้อมูลรายการ หนังสือ ทั้งหมด
    /// </summary>
    /// <returns>ข้อมูลรายการ หนังสือ ทั้งหมด</returns>
    /// <response code="200">ส่งข้อมูล หนังสือ ทั้งหมด</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="422">เกิดข้อผิดพลาดจากข้อมูลที่ส่งมา</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpGet]
    [ProducesResponseType(typeof(List<BookRequest>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status422UnprocessableEntity)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Get()
    {
        var books = await _bookRepository.Fetch();
            //throw new Exception("Exception while fetching all the students from the storage.");
        _logger.LogInfo($"{StatusCodes.Status200OK} - {books.Count()} retrieved");
        return Ok(books.Select(m => m.ToResponse()).ToList());
    }


    /// <summary>
    /// แสดงข้อมูล หนังสือ ที่ระบุ ตามประเภทหนังสือ
    /// </summary>
    /// <param name="BooktypeId"></param>
    /// <returns>ข้อมูล หนังสือ ที่ระบุตามประเภทหนังสือ</returns>
    /// <response code="200">ส่งข้อมูล หนังสือ ที่ระบุ ตามประเภทหนังสือ</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="404">ไม่พบข้อมูลที่ระบุ ตามประเภทหนังสือ</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpGet("Booktype/{BooktypeId}")]
    [ProducesResponseType(typeof(BookRequest), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetByBooktypeId(long BooktypeId)
    {
        var books = await _bookRepository.FetchByBooktypeId(BooktypeId);
        if (books.Count() == 0)
        {
            _logger.LogError($"{StatusCodes.Status404NotFound} - Not found");
            return NotFound();
        }

        _logger.LogInfo($"{StatusCodes.Status200OK} - Get the Item");
        return Ok(books.Select(m => m.ToResponse()).ToList());
    }

    /// <summary>
    /// แสดงข้อมูล หนังสือ ที่ระบุ
    /// </summary>
    /// <param name="id"></param>
    /// <returns>ข้อมูล หนังสือ ที่ระบุ</returns>
    /// <response code="200">ส่งข้อมูล หนังสือ ที่ระบุ</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="404">ไม่พบข้อมูลที่ระบุ</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpGet("{id}")]
    [ProducesResponseType(typeof(BookRequest), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Get(long id)
    {
        var books = await _bookRepository.Find(id);
        if (books is null)
        {
            _logger.LogError($"{StatusCodes.Status404NotFound} - Not found");
            return NotFound();
        }

        _logger.LogInfo($"{StatusCodes.Status200OK} - Get the Item");
        return Ok(books.ToResponse());
    }


    /// <summary>
    /// เพิ่มข้อมูล หนังสือ
    /// </summary>
    /// <param name="model"></param>
    /// <returns>ข้อมูล หนังสือ ที่เพิ่มแล้ว</returns>
    /// <response code="201">ส่งข้อมูล หนังสือ ที่เพิ่มแล้ว</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="422">เกิดข้อผิดพลาดจากข้อมูลที่ส่งมา</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpPost]
    [ProducesResponseType(typeof(BookResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status422UnprocessableEntity)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Post([FromBody] BookRequest model)
    {
        var validationResult = await _validator.ValidateAsync(model);
        if (!validationResult.IsValid) 
        {
            _logger.LogError($"{StatusCodes.Status422UnprocessableEntity} - Invalid input");
            return UnprocessableEntity(validationResult.ToDictionary());
        }

        var book = await _bookRepository.Create(model);
        _logger.LogInfo($"{StatusCodes.Status201Created} - The item was created");
        return CreatedAtAction(nameof(Post), book.ToResponse());
    }


    /// <summary>
    /// แก้ไขข้อมูล หนังสือ
    /// </summary>
    /// <param name="id"></param>
    /// <param name="model"></param>
    /// <returns>ข้อมูล หนังสือ ที่แก้ไขแล้ว</returns>
    /// <response code="200">ส่ง หนังสือ ที่แก้ไขแล้ว</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="404">ไม่พบข้อมูลที่ระบุ</response>                        
    /// <response code="422">เกิดข้อผิดพลาดจากข้อมูลที่ส่งมา</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpPut("{id}")]
    [ProducesResponseType(typeof(BookResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status422UnprocessableEntity)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Put(long id, [FromBody] BookRequest model)
    {
        var validationResult = await _validator.ValidateAsync(model);
        if (!validationResult.IsValid) 
        {
            _logger.LogError($"{StatusCodes.Status422UnprocessableEntity} - Invalid input");
            return UnprocessableEntity(validationResult.ToDictionary());
        }

        var source = await _bookRepository.Find(id);
        if (source is null)
        {
            _logger.LogError($"{StatusCodes.Status404NotFound} - Not found");
            return NotFound();
        }

        var book = await _bookRepository.Update(source, model);
        _logger.LogInfo($"{StatusCodes.Status200OK} - The item was updated");
        return Ok(book.ToResponse());
    }


    /// <summary>
    /// ลบข้อมูล  หนังสือ
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    /// <response code="204">ลบข้อมูล หนังสือ</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="404">ไม่พบข้อมูลที่ระบุ</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Delete(long id)
    {

        var book = await _bookRepository.Find(id);
        if (book is null)
        {
            _logger.LogError($"{StatusCodes.Status404NotFound} - Not found");
            return NotFound();
        }

        await _bookRepository.Delete(book);
        _logger.LogInfo($"{StatusCodes.Status204NoContent} - The item was deleted");
        return NoContent();
  
    }
}
