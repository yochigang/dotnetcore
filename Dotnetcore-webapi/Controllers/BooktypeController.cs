using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Dotnetcore.webapi.Data.Requests;
using Dotnetcore.webapi.Data.Responses;
using Dotnetcore.webapi.Extensions;
using Dotnetcore.webapi.Interfaces;
using Dotnetcore.webapi.Services;

namespace Dotnetcore.webapi.Controllers;

[Route("api/[controller]")]
[ApiController]
[ApiExplorerSettings(GroupName = "Booktype")]
[Produces("application/json")]
public class BooktypeController : ControllerBase
{
    private readonly IBooktypeRepository _booktypeRepository;
    private readonly IValidator<BooktypeRequest> _validator;
    private readonly ILoggerManagerService _logger;

    public BooktypeController(IBooktypeRepository booktypeRepository,
        IValidator<BooktypeRequest> validator,
        ILoggerManagerService logger
        )
    {
        _booktypeRepository = booktypeRepository;
        _validator = validator;
        _logger = logger;
    }


    /// <summary>
    /// แสดงข้อมูลรายการ ประเภทหนังสือ ทั้งหมด
    /// </summary>
    /// <returns>ข้อมูลรายการ ประเภทหนังสือ ทั้งหมด</returns>
    /// <response code="200">ส่งข้อมูล ประเภทหนังสือ ทั้งหมด</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="422">เกิดข้อผิดพลาดจากข้อมูลที่ส่งมา</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpGet]
    [ProducesResponseType(typeof(List<BooktypeRequest>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status422UnprocessableEntity)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Get()
    {
        var cats = await _booktypeRepository.Fetch();
            //throw new Exception("Exception while fetching all the students from the storage.");
        _logger.LogInfo($"{StatusCodes.Status200OK} - {cats.Count()} retrieved");
        return Ok(cats.Select(m => m.ToResponse()).ToList());
    }

    /// <summary>
    /// แสดงข้อมูล ประเภทหนังสือ ที่ระบุ
    /// </summary>
    /// <param name="id"></param>
    /// <returns>ข้อมูล ประเภทหนังสือ ที่ระบุ</returns>
    /// <response code="200">ส่งข้อมูล ประเภทหนังสือ ที่ระบุ</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="404">ไม่พบข้อมูลที่ระบุ</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpGet("{id}")]
    [ProducesResponseType(typeof(BooktypeRequest), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Get(long id)
    {
        var booktype = await _booktypeRepository.Find(id);
        if (booktype is null)
        {
            _logger.LogError($"{StatusCodes.Status404NotFound} - Not found");
            return NotFound();
        }

        _logger.LogInfo($"{StatusCodes.Status200OK} - Get the Item");
        return Ok(booktype.ToResponse());
    }


    /// <summary>
    /// เพิ่มข้อมูล ประเภทหนังสือ
    /// </summary>
    /// <param name="model"></param>
    /// <returns>ข้อมูล ประเภทหนังสือ ที่เพิ่มแล้ว</returns>
    /// <response code="201">ส่งข้อมูล ประเภทหนังสือ ที่เพิ่มแล้ว</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="422">เกิดข้อผิดพลาดจากข้อมูลที่ส่งมา</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpPost]
    [ProducesResponseType(typeof(BooktypeResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status422UnprocessableEntity)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Post([FromBody] BooktypeRequest model)
    {
        var validationResult = await _validator.ValidateAsync(model);
        if (!validationResult.IsValid) 
        {
            _logger.LogError($"{StatusCodes.Status422UnprocessableEntity} - Invalid input");
            return UnprocessableEntity(validationResult.ToDictionary());
        }

        var booktype = await _booktypeRepository.Create(model);
        _logger.LogInfo($"{StatusCodes.Status201Created} - The item was created");
        return CreatedAtAction(nameof(Post), booktype.ToResponse());
    }


    /// <summary>
    /// แก้ไขข้อมูล ประเภทหนังสือ
    /// </summary>
    /// <param name="id"></param>
    /// <param name="model"></param>
    /// <returns>ข้อมูล ประเภทหนังสือ ที่แก้ไขแล้ว</returns>
    /// <response code="200">ส่ง ประเภทหนังสือ ที่แก้ไขแล้ว</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="404">ไม่พบข้อมูลที่ระบุ</response>                        
    /// <response code="422">เกิดข้อผิดพลาดจากข้อมูลที่ส่งมา</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpPut("{id}")]
    [ProducesResponseType(typeof(BooktypeResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status422UnprocessableEntity)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Put(long id, [FromBody] BooktypeRequest model)
    {
        var validationResult = await _validator.ValidateAsync(model);
        if (!validationResult.IsValid) 
        {
            _logger.LogError($"{StatusCodes.Status422UnprocessableEntity} - Invalid input");
            return UnprocessableEntity(validationResult.ToDictionary());
        }

        var source = await _booktypeRepository.Find(id);
        if (source is null)
        {
            _logger.LogError($"{StatusCodes.Status404NotFound} - Not found");
            return NotFound();
        }

        var booktype = await _booktypeRepository.Update(source, model);
        _logger.LogInfo($"{StatusCodes.Status200OK} - The item was updated");
        return Ok(booktype.ToResponse());
    }


    /// <summary>
    /// ลบข้อมูล  ประเภทหนังสือ
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    /// <response code="204">ลบข้อมูล ประเภทหนังสือ</response>
    /// <response code="401">ไม่มีสิทธิ์ใช้งาน</response>
    /// <response code="404">ไม่พบข้อมูลที่ระบุ</response>
    /// <response code="500">เกิดข้อผิดพลาดในระบบ</response>
    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Delete(long id)
    {

        var booktype = await _booktypeRepository.Find(id);
        if (booktype is null)
        {
            _logger.LogError($"{StatusCodes.Status404NotFound} - Not found");
            return NotFound();
        }

        await _booktypeRepository.Delete(booktype);
        _logger.LogInfo($"{StatusCodes.Status204NoContent} - The item was deleted");
        return NoContent();
  
    }
}
