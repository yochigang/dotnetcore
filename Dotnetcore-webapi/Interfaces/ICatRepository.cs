using Dotnetcore.webapi.Data.Models;
using Dotnetcore.webapi.Data.Requests;

namespace Dotnetcore.webapi.Interfaces;

public interface ICatRepository
{
    Task<IEnumerable<Cat>> Fetch();

    Task<Cat?> Find(long id);

    Task<Cat> Create(CatRequest model);

    Task<Cat> Update(Cat entity, CatRequest model);

    Task<long> Delete(Cat entity);
}
