using Dotnetcore.webapi.Data.Models;
using Dotnetcore.webapi.Data.Requests;

namespace Dotnetcore.webapi.Interfaces;

public interface IBooktypeRepository
{
    Task<IEnumerable<Booktype>> Fetch();

    Task<Booktype?> Find(long id);

    Task<Booktype> Create(BooktypeRequest model);

    Task<Booktype> Update(Booktype entity, BooktypeRequest model);

    Task<long> Delete(Booktype entity);
}