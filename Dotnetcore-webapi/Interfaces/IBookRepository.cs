using Dotnetcore.webapi.Data.Models;
using Dotnetcore.webapi.Data.Requests;

namespace Dotnetcore.webapi.Interfaces;

public interface IBookRepository
{
    Task<IEnumerable<Book>> Fetch();
    Task<IEnumerable<Book>> FetchByBooktypeId(long BooktypeId);
    Task<Book?> Find(long id);

    Task<Book> Create(BookRequest model);

    Task<Book> Update(Book entity, BookRequest model);

    Task<long> Delete(Book entity);
}