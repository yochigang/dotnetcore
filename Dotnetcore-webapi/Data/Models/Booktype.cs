using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dotnetcore.webapi.Data.Models;

public class Booktype : ITimeStamped
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }

    public required string BooktypeName { get; set; }

    public ICollection<Book> Books { get; set; } = new List<Book>();

    public DateTime CreatedAt { get; set; }

    public DateTime UpdatedAt { get; set; }
}
