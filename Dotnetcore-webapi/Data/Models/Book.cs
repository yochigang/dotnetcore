using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dotnetcore.webapi.Data.Models;

public class Book : ITimeStamped
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }

    public required string BookName { get; set; }

    [ForeignKey("Booktypes")]
    public long BooktypeId { get; set; }
    public Booktype Booktype { get; set; } = null!;

    public DateTime CreatedAt { get; set; }

    public DateTime UpdatedAt { get; set; }
}
