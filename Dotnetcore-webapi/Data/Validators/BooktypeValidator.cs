using Dotnetcore.webapi.Data.Requests;
using FluentValidation;

namespace Dotnetcore.webapi.Data.Validators;

public class BooktypeValidator : AbstractValidator<BooktypeRequest> 
{
    private readonly ApplicationDbContext _context;

    public BooktypeValidator(ApplicationDbContext context)
    {
        _context = context;

        RuleFor(m => m.Id);
        RuleFor(m => m.BooktypeName).NotNull().NotEmpty();
   
    }

    private bool NotExist(long id)
    {
        var entities = _context.Cats
            .Where(m => m.Id == id)
            .ToList();

        return entities.Count == 0;
    }
}
