using Dotnetcore.webapi.Data.Requests;
using FluentValidation;

namespace Dotnetcore.webapi.Data.Validators;

public class CatValidator : AbstractValidator<CatRequest> 
{
    private readonly ApplicationDbContext _context;

    public CatValidator(ApplicationDbContext context)
    {
        _context = context;

        RuleFor(m => m.Id);
        RuleFor(m => m.CatName).NotNull().NotEmpty();
        RuleFor(m => m.CatDate).NotNull().NotEmpty();
   
    }

    private bool NotExist(long id)
    {
        var entities = _context.Cats
            .Where(m => m.Id == id)
            .ToList();

        return entities.Count == 0;
    }
}
