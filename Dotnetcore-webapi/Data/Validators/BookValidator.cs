using Dotnetcore.webapi.Data.Requests;
using FluentValidation;

namespace Dotnetcore.webapi.Data.Validators;

public class BookValidator : AbstractValidator<BookRequest> 
{
    private readonly ApplicationDbContext _context;

    public BookValidator(ApplicationDbContext context)
    {
        _context = context;

        RuleFor(m => m.Id);
        RuleFor(m => m.BookName).NotNull().NotEmpty();
        RuleFor(m => m.BooktypeId).Cascade(CascadeMode.Stop).NotNull().NotEmpty().Must(BooktypeIdExist);
   
    }

    private bool NotExist(long id)
    {
        var entities = _context.Cats
            .Where(m => m.Id == id)
            .ToList();

        return entities.Count == 0;
    }

        private bool BooktypeIdExist(long id)
    {
        var entity = _context.Booktypes.Find(id);
        return entity is not null;
    }

}
