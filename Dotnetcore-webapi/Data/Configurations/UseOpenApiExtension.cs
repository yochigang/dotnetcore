using Microsoft.AspNetCore.Rewrite;

namespace Dotnetcore.webapi.Configurations;

public static class UseOpenApiExtension
{
    public static void UseOpenApi(this WebApplication app,
        IConfiguration configuration)
    {
        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI(options => 
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Dotnetcore WEBAPI V1");
                options.RoutePrefix = "api-docs";
                
                // options.OAuthAppName(configuration.GetValue<string>("Keycloak:ClientId"));
                // options.OAuthClientId(configuration.GetValue<string>("Keycloak:ClientId"));
                // options.OAuthClientSecret(configuration.GetValue<string>("Keycloak:ClientSecret"));
                // options.OAuthUseBasicAuthenticationWithAccessCodeGrant();
                // options.OAuthUsePkce();
            });
            app.UseRewriter(new RewriteOptions().AddRedirect("^$", "api-docs"));
        }
    }
}
