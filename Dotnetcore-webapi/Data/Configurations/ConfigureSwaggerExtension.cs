using System.Reflection;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.OpenApi.Models;

namespace Dotnetcore.webapi.Configurations;

public static class ConfigureSwaggerExtension
{
    public static void ConfigureSwagger(this IServiceCollection services, IConfiguration configuration)
    {
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "DotnetCore WEBAPI",
                Version = "v1",
                Description = "รายละเอียดข้อมูลและการเรียกใช้งานระบบ API"
            });

            options.TagActionsBy(api =>
            {
                if (api.GroupName is not null)
                {
                    return new [] { api.GroupName };
                }

                if (api.ActionDescriptor is ControllerActionDescriptor controllerActionDescriptor)
                {
                    return new [] { controllerActionDescriptor.ControllerName };
                }

                throw new InvalidOperationException("Unable to determine tag for endpoint.");
            });
            options.DocInclusionPredicate((name, api) => true);
            options.SupportNonNullableReferenceTypes();

            // Set the comments path for the Swagger JSON and UI.
            var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));

            // Include 'SecurityScheme' to use OpenId Connect
            // var AuthorityUrl = new Uri(configuration.GetValue<string>("Keycloak:Authority")!);
            // var securityScheme = new OpenApiSecurityScheme
            // {
            //     Type = SecuritySchemeType.OAuth2,
            //     Flows = new OpenApiOAuthFlows
            //     {
            //         AuthorizationCode = new OpenApiOAuthFlow
            //         {
            //             AuthorizationUrl = new Uri(AuthorityUrl, "protocol/openid-connect/auth"),
            //             TokenUrl = new Uri(AuthorityUrl, "protocol/openid-connect/token"),
            //             RefreshUrl = new Uri(AuthorityUrl, "protocol/openid-connect/token"),
            //             Scopes = new Dictionary<string, string>
            //             {
            //                 ["vul_api"] = "Dotnetcore API"
            //             }
            //         }
            //     },
            //     Description = "Dotnetcore Server OpenId Security Scheme",
            //     Reference = new OpenApiReference
            //     {
            //         Id = "DatafarmAuth",
            //         Type = ReferenceType.SecurityScheme
            //     }
            // };

            // options.AddSecurityDefinition("DatafarmAuth", securityScheme);
            // options.AddSecurityRequirement(new OpenApiSecurityRequirement
            // {
            //     { securityScheme, new string[] { "vul_api" } }
            // });
        });
    }
}
