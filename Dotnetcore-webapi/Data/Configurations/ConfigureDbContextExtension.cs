using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Dotnetcore.webapi.Data;

namespace Dotnetcore.webapi.Configurations;

public static class ConfigureDbContextExtension
{
    public static void ConfigureDbContext(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<ApplicationDbContext>(options =>
        {
            options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"),
                sql =>
                {
                    sql.MigrationsAssembly(typeof(Program).GetTypeInfo().Assembly.GetName().Name);
                    sql.CommandTimeout((int)TimeSpan.FromMinutes(1).TotalSeconds);
                });
        });
    }
}
