using Dotnetcore.webapi.Interfaces;
using Dotnetcore.webapi.Repositories;

namespace Dotnetcore.webapi.Configurations;

public static class ConfigureDependentcyInjectionExtension
{
    public static void ConfigureDependencyInjection(this IServiceCollection services)
    {
        // Add dependency injection
        services.AddScoped<ICatRepository, CatRepository>();
        services.AddScoped<IBooktypeRepository, BooktypeRepository>();
        services.AddScoped<IBookRepository, BookRepository>();

    }
}
