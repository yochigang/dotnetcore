using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Dotnetcore.webapi.Data.Validators;
using Dotnetcore.webapi.Data.Models;

namespace Dotnetcore.webapi.Configurations;

public static class ConfigureValidationExtension
{
    public static void ConfigureValidation(this IServiceCollection services)
    {
        services.Configure<ApiBehaviorOptions>(options =>
        {
            options.SuppressModelStateInvalidFilter = true;
        });

        services.AddFluentValidationAutoValidation(fv =>
        {
            fv.DisableDataAnnotationsValidation = true;
        });

        services.AddValidatorsFromAssemblyContaining<CatValidator>();
        services.AddValidatorsFromAssemblyContaining<BooktypeValidator>();
        services.AddValidatorsFromAssemblyContaining<BookValidator>();
    }
}
