using Microsoft.EntityFrameworkCore;

namespace Dotnetcore.webapi.Data.ModelConfigurations;

interface IConfiguration
{
    void Apply(ModelBuilder builder);
}
