using Microsoft.EntityFrameworkCore;
using Dotnetcore.webapi.Data.Models;

namespace Dotnetcore.webapi.Data.ModelConfigurations;

public class BooktypeConfig : IConfiguration
{
    public void Apply(ModelBuilder builder)
    {
        builder.Entity<Booktype>()
            .HasIndex(m => m.BooktypeName)
            .IsUnique(true);
    }
}
