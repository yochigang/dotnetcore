using System;

namespace Dotnetcore.webapi.Data;

public interface ITimeStamped
{
    DateTime CreatedAt { get; set; }
    
    DateTime UpdatedAt { get; set; }
}
