using Dotnetcore.webapi.Data.Models;
using Dotnetcore.webapi.Data.Responses;
using Dotnetcore.webapi.Repositories;

namespace Dotnetcore.webapi.Extensions;

public static class ResponseExtension
{
    public static CatResponse ToResponse(this Cat entity)
    {
        return new CatResponse
        {
            Id = entity.Id,
            CatName = entity.CatName,
            CatDate = entity.CatDate
        };
    }

    public static BooktypeResponse ToResponse(this Booktype entity)
    {
        return new BooktypeResponse
        {
            Id = entity.Id,
            BooktypeName = entity.BooktypeName,
        };
    }

    public static BookResponse ToResponse(this Book entity)
    {
        return new BookResponse
        {
            Id = entity.Id,
            BookName = entity.BookName,
            BooktypeId = entity.BooktypeId,
            Booktype = new BooktypeResponse
            {
                Id = entity.Id,
                BooktypeName = entity.Booktype.BooktypeName,
            }
        };
    }  
}
