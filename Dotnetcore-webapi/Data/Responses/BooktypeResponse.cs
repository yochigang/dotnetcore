using Dotnetcore.webapi.Data.Requests;

namespace Dotnetcore.webapi.Data.Responses;

public class BooktypeResponse 
{

    public long Id { get; set; }

    public required string BooktypeName { get; set; }
}