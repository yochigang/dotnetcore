using Microsoft.EntityFrameworkCore;
using Dotnetcore.webapi.Data.Models;
using Dotnetcore.webapi.Data.ModelConfigurations;

namespace Dotnetcore.webapi.Data;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
        //
    }
    public DbSet<Cat> Cats => Set<Cat>();
    public DbSet<Booktype> Booktypes => Set<Booktype>();
    public DbSet<Book> Books => Set<Book>();

    // model configurations
    protected void Configuration(ModelBuilder builder)
    {
        new BooktypeConfig().Apply(builder);

    }

    // model data seeders
    protected void DataSeeder(ModelBuilder builder)
    {

    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        // adds model configurations.
        Configuration(builder);

        // adds model data seeders
        DataSeeder(builder);
    }

    public override int SaveChanges()
    {
        TimeStamped();

        return base.SaveChanges();
    }

    public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        TimeStamped();

        return await base.SaveChangesAsync(cancellationToken);
    }

    private void TimeStamped()
    {
        var entityEntries = this.ChangeTracker.Entries();

        entityEntries.Where(x => (
            x.State == EntityState.Added || x.State == EntityState.Modified) &&
            x.Entity is not null &&
            x.Entity as ITimeStamped is not null
        ).ToList()
            .ForEach(x =>
            {
                var entityEntry = x.Entity as ITimeStamped;

                if (x.State == EntityState.Added)
                {
                    entityEntry!.CreatedAt = DateTime.UtcNow;
                }

                entityEntry!.UpdatedAt = DateTime.UtcNow;         
            });
    }
}
