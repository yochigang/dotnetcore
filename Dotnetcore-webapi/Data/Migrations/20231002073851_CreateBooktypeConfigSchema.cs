﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Dotnetcore.webapi.Data.Migrations
{
    /// <inheritdoc />
    public partial class CreateBooktypeConfigSchema : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Booktypes_BooktypeName",
                table: "Booktypes",
                column: "BooktypeName",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Booktypes_BooktypeName",
                table: "Booktypes");
        }
    }
}
