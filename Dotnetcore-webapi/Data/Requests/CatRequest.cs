namespace Dotnetcore.webapi.Data.Requests;

public class CatRequest
{
    public long Id { get; set; }

    public required string CatName { get; set; }

    public DateTime CatDate { get; set; }
}
