
using Dotnetcore.webapi.Data.Responses;
using Dotnetcore.webapi.Repositories;

namespace Dotnetcore.webapi.Data.Requests;

public class BookRequest
{
    public long Id { get; set; }
    public required string BookName { get; set; }
    public long BooktypeId { get; set; }
    public required BooktypeResponse Booktype { get; set; }
}    
    
