namespace Dotnetcore.webapi.Data.Requests;

public class BooktypeRequest
{
    public long Id { get; set; }

    public required string BooktypeName { get; set; }
}   