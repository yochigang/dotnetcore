using NLog;

 

namespace Dotnetcore.webapi.Services;

 

public class LoggerManagerService : ILoggerManagerService

{

    private static readonly NLog.ILogger   _logger = LogManager.GetCurrentClassLogger();

 

    public void LogDebug(string message) => _logger.Debug(message);

 

    public void LogError(string message) => _logger.Error(message);

 

    public void LogInfo(string message) => _logger.Info(message);

 

    public void LogWarn(string message) => _logger.Warn(message);

}

 

    public interface ILoggerManagerService

    {

        void LogInfo(string message);

 

        void LogWarn(string message);

 

        void LogDebug(string message);

        

        void LogError(string message);

    }